import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class riskWidget extends StatefulWidget {
  riskWidget({Key? key}) : super(key: key);

  @override
  _riskWidgetState createState() => _riskWidgetState();
}

class _riskWidgetState extends State<riskWidget> {
  var risks = [
    'โรคทางเดินหายใจเรื้อรัง',
    'โรคหัวใจและหลอดเลือด',
    'โรคไตวายเรื้อรัง',
    'โรคหลอดเลือดสมอง',
    'โรคอ้วน',
    'โรคมะเร็ง',
    'โรคเบาหวาน'
  ];
  var risksValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Risk')),
      body: ListView(
        children: [
          CheckboxListTile(
              value: risksValues[0],
              title: Text(risks[0]),
              onChanged: (newValue) {
                setState(() {
                  risksValues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: risksValues[1],
              title: Text(risks[1]),
              onChanged: (newValue) {
                setState(() {
                  risksValues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: risksValues[2],
              title: Text(risks[2]),
              onChanged: (newValue) {
                setState(() {
                  risksValues[2] = newValue!;
                });
              }),
              CheckboxListTile(
              value: risksValues[3],
              title: Text(risks[3]),
              onChanged: (newValue) {
                setState(() {
                  risksValues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: risksValues[4],
              title: Text(risks[4]),
              onChanged: (newValue) {
                setState(() {
                  risksValues[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: risksValues[5],
              title: Text(risks[5]),
              onChanged: (newValue) {
                setState(() {
                  risksValues[5] = newValue!;
                });
              }),
              CheckboxListTile(
              value: risksValues[6],
              title: Text(risks[6]),
              onChanged: (newValue) {
                setState(() {
                  risksValues[6] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saveRisk();
                Navigator.pop(context);
              },
              child: const Text('Save')),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadRisk();
  }

  Future<void> _loadRisk() async {
    var pref = await SharedPreferences.getInstance();
    var strQuestionValue = pref.getString('risk_values') ??
        '[false, false, false, false, false, false, false]';
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        risksValues[i] = (arrStrQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveRisk() async {
    var pref = await SharedPreferences.getInstance();
    pref.setString('risk_values', risksValues.toString());
  }
}